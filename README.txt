CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Maintainers


INTRODUCTION
------------

The module allows to use an image to represent the color/size (options)
instead of the dropdown-list of the "add-to-cart-form". It converts select
list to attached to product images. The product's info (SKU, price)
reloads on image clicks.
 * For a full description of the module, visit the project page:
https://www.drupal.org/sandbox/petu/2301343
 * To submit bug reports and feature suggestions, or to track changes:
https://www.drupal.org/node/add/project-issue/2301343


REQUIREMENTS
------------

The module requires the following modules:
 * Commerce cart (https://www.drupal.org/project/commerce)


INSTALLATION
------------

* Install and enable the module (as usual module):
https://www.drupal.org/documentation/install/modules-themes/modules-7


CONFIGURATION
-------------

* Go to configuration page
http://your-site-url.domain/admin/commerce/config/options_as_images
for configuration.
 * Save settings
 * Make changes to your theme's css file if required.


MAINTAINERS
-----------

Current maintainers:
 * Peter B. Lozovitskiy (petu) - https://www.drupal.org/u/petu
This project has been sponsored by:
 * Bumerang ad company (http://ipss.ru/)
